module ProjectsHelper
  def check_user
    if current_user.admin_id.nil?
      @id = current_user.id
    else
      @id = current_user.admin_id
    end
  end
end
