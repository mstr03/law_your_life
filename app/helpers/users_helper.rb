module UsersHelper
  def register_number
    @number = @user.krs_number
    @basic_info = HTTParty.get("https://api-v3.mojepanstwo.pl/dane/krs_podmioty/#{@number}.json?layers[]=reprezentacja&layers[]=wspolnicy").parsed_response
  end
end
