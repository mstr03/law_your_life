class ProjectsController < ApplicationController
  include ProjectsHelper
  before_action :check_user, :require_login
  before_action :set_projects, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.where(user_id: @id)
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @project.todos
    @todo = Todo.new
  end

  # GET /projects/new
  def new
    @project = Project.new
    3.times do
      @project.todos.build
    end
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.create(project_params)
      if @project.save
        redirect_to project_path(@project)
      else
        render 'new'
      end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_projects
      @project = Project.where(id: params[:id], user_id: @id).first
      if @project.nil?
        redirect_to projects_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params[:project][:user_id] = current_user.id
      params.require(:project).permit(:title, :description, :user_id, :due_date, todos_attributes: [:id, :status, :description, :due_date])
    end
end
