class SessionController < ApplicationController
  before_action :check_login, only: [:new, :create]

  def new
  end

  def create
    user = User.find_by_email(params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to office_home_url
      flash[:notice] = "Welcome!"
    else
      flash[:error] = "Invalid email and/or password"
      render "new"
    end
  end

  def destroy
    log_out if logged_in
    redirect_to root_url
  end
end
