class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionHelper
  helper_method :current_user

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  private

  def require_login
    unless logged_in
      redirect_to login_url
    end
  end

  def check_login
    if logged_in
      redirect_to office_home_url
    end
  end

  def only_admin
    unless logged_in && current_user.role == "admin"
      redirect_to office_home_url
    end
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end
end
