class IntroController < ApplicationController
  before_action :check_login, only: :home

  def home
  end

  def about
  end

  def features
  end

  def contact
  end
end
