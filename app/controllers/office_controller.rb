class OfficeController < ApplicationController
  include ProjectsHelper
  before_action :only_admin, only: [:newClient, :createClient, :clients]
  before_action :require_login, :check_user

  def home
  end

  def newClient
    @user = User.new
  end

  def createClient
    @user = User.new(user_params)
    @user[:role] = 'client'
    @user.admin_id = current_user.id
    @user.validate_krs = true
    if @user.save
      redirect_to office_home_url
      flash.notice = "New client created"
    else
      render 'newClient'
    end
  end

  def clients
    @users = User.where(role: 'client', admin_id: current_user.id)
  end

  def calendar
    @project = Project.where(user_id: @id).pluck(:id)
    @todos = Todo.where(project_id: @project)
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :krs_number)
  end

end
