class TodosController < ApplicationController
  before_action :set_todo, only: [:show, :edit, :update, :destroy]

  # GET /todos
  # GET /todos.json
  def index
    @todos = Todo.all
    @todo = Todo.new
  end

  # GET /todos/1
  # GET /todos/1.json
  def show
    @project = @todo.project_id
  end

  # GET /todos/new
  def new
    @todo = Todo.new
    @project = @todo.project_id
  end

  # GET /todos/1/edit
  def edit
    @project = @todo.project_id
  end

  # POST /todos
  # POST /todos.json
  def create
    @todo = Todo.new(todo_params)
    @project = @todo.project_id
    respond_to do |format|
      if @todo.save
        format.html { redirect_to "/projects/#{@project}", notice: 'Todo was successfully created.' }
        format.json { render :show, status: :created, location: @todo }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /todos/1
  # PATCH/PUT /todos/1.json
  def update
    @project = @todo.project_id
    respond_to do |format|
      if @todo.update(todo_params)
        format.html { redirect_to "/projects/#{@project}", notice: 'Todo was successfully updated.' }
        format.json { render :show, status: :ok, location: @todo }
      else
        format.html { render :edit }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todos/1
  # DELETE /todos/1.json
  def destroy
    @project = @todo.project_id
    @todo.destroy
    respond_to do |format|
      format.html { redirect_to "/projects/#{@project}", notice: 'Todo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo = Todo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_params
      params.require(:todo).permit(:project_id, :status, :description, :due_date)
    end
end
