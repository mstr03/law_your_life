class UsersController < ApplicationController
  include UsersHelper
  before_action :check_login, only: :new
  before_action :find_user, only: :show

  def index
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user[:role] = 'admin'
    @user.validate_krs = false
    if @user.save
      log_in(@user)
      flash[:notice] = "Welcome!"
      redirect_to office_home_url
    else
      render 'new'
    end
  end

  def show
    register_number
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      redirect_to office_home_url
      flash[:notice] = "Client deleted"
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def find_user
    @user = User.find(params[:id])
  end
end
