class User < ApplicationRecord
  attr_accessor :validate_krs
  has_many :clients, class_name: "User",
                     foreign_key: "admin_id"
  belongs_to :admin, class_name: "User", optional: true
  has_many :projects, dependent: :destroy,
                      inverse_of: :user
  before_save { self.email = email.downcase }
  ROLES = %i[client admin]
  VALID_EMAIL = /\A[^@\s]+@([^@.\s]+\.)*[^@.\s]+\z/i
  PASSWORD = /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])/
  validates :password,   length: { minimum: 6 },
                         format: { with: PASSWORD,
                                   message: "should have at least one number and one uppercase letter" }
  validates :email,      presence: true,
                         format: { with: VALID_EMAIL },
                         uniqueness: { case_sensitive: false }
  validates :krs_number, uniqueness: true,
                         numericality: { only_integer: true,
                                         greater_than: 0},
                         if: :validate_krs?
  has_secure_password

  def validate_krs?
    validate_krs == true
  end
end
