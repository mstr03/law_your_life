class Todo < ApplicationRecord
  belongs_to :project, optional: true

  def start_time
       self.due_date
  end
end
