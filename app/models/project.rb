class Project < ApplicationRecord
  validates :title,       presence: true
  validates :description, presence: true
  has_many :todos, dependent: :destroy
  accepts_nested_attributes_for :todos, allow_destroy: true
  belongs_to :user, optional: true,
                    inverse_of: :projects
end
