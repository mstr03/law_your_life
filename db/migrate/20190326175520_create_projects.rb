class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :title
      t.text :description
      t.datetime :due_date

      t.references :user, index: true

      t.timestamps
    end
  end
end
