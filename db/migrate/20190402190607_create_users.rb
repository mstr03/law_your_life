class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.references :admin, index: true
      t.string :email
      t.string :password_digest
      t.string :role

      t.timestamps
    end
  end
end
