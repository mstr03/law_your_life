class CreateTodos < ActiveRecord::Migration[5.2]
  def change
    create_table :todos do |t|
      t.boolean :status
      t.text :description
      t.datetime :due_date

      t.references :project, index: true, foreign_key: true

      t.timestamps
    end
  end
end
