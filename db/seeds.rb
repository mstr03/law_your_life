# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.new(email: "strzelczyk.michal@yahoo.com",
                password: "LawYourLife1",
                password_confirmation: "LawYourLife1",
                role: 'admin')
client = User.new(email: "client@example.com",
                  password: "Client1",
                  password_confirmation: "Client1",
                  role: 'client',
                  admin_id: 1)
user.save
client.save
