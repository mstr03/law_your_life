class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

  Rack::Attack.throttle('limit sign up', limit: 3, period: 3.minutes) do |req|
    req.ip if req.path == '/users' && req.post?
  end

  Rack::Attack.throttle('limit login', limit: 5, period: 2.minutes) do |req|
    req.ip if req.path == '/login' && req.post?
  end

  Rack::Attack.safelist('allow from localhost') do |req|
    '127.0.0.1' == req.ip || '::1' == req.ip
  end
end
