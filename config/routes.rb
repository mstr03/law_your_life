Rails.application.routes.draw do
  resources :todos
  resources :projects
  root 'intro#home'
  get 'home',      to: 'intro#home',      as: 'home'
  get 'about',     to: 'intro#about',     as: 'about'
  get 'features',  to: 'intro#features',  as: 'features'
  get 'contact',   to: 'intro#contact',   as: 'contact'
  resources :users
  get 'login',     to: 'session#new',     as: 'login'
  post 'login',    to: 'session#create',  as: 'logging'
  delete 'logout', to: 'session#destroy', as: 'logout'
  get 'office/home'
  get 'office/newClient'
  post 'office/createClient'
  get 'office/clients'
  get 'office/calendar'
end
