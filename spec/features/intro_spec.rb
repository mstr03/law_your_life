require 'rails_helper'

RSpec.feature "Intro", type: :feature do
  describe "visit page and check title for" do
    it 'home' do
      visit '/home'
      expect(page).to have_title("Home | Law Your Life")
    end

    it 'about' do
      visit '/about'
      expect(page).to have_title("About | Law Your Life")
    end

    it 'features' do
      visit '/features'
      expect(page).to have_title("Features | Law Your Life")
    end

    it 'contact' do
      visit '/contact'
      expect(page).to have_title("Contact | Law Your Life")
    end
  end
end
