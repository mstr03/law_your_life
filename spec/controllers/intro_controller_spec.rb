require 'rails_helper'

RSpec.describe IntroController, type: :controller do
  describe "GET intro pages" do
    it "render home template" do
      get :home
      expect(response).to render_template("home")
      expect(response.successful?)
    end

    it "render about template" do
      get :about
      expect(response).to render_template("about")
      expect(response.successful?)
    end

    it "render features template" do
      get :features
      expect(response).to render_template("features")
      expect(response.successful?)
    end

    it "render contact template" do
      get :contact
      expect(response).to render_template("contact")
      expect(response.successful?)
    end
  end
end
